package main

import (
	"flag"
	"fmt"
	serial "github.com/tarm/goserial"
	"go-osdp/src/osdp"
	"log"
	"os"
	"time"
)

var (
	version   = "Version 0.0.1 build 2020.11.26"
	port      = flag.String("port", "COM1", "Serial port name COM1,COM2, etc. ")
	idCur     = flag.Int("idCur", 1, "Current ID of Peripheral Device  0-126")
	idSet     = flag.Int("idSet", 1, "Current ID of Peripheral Device  0-126")
	speedConn = flag.Int("speedConn", 9600, "Connection speed")
	speedSet  = flag.Int("speedSet", 9600, "Connection speed")
	autoSpeed = flag.Bool("autoSpeed", false, "Brute force speeds {1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200}")
	autoId    = flag.Bool("autoId", false, "Brute force id's {1-127}")
	crcMode   = flag.Bool("crc16", false, "Error checking LRC or CRC16")
	info      = flag.Bool("info", false, "Info about program")
)

func main() {
	flag.Parse()
	if *info {
		showInfo()
		os.Exit(0)
	}
	if err := checkValues(); err != nil {
		fmt.Printf("ERROR: %s\n", err)
		os.Exit(1)
	}

	//Brute force mode
	if *autoId {
		for i := 0; i < 0x7F; i++ {
			buf, err := makePacket(uint8(i), *speedSet)
			fmt.Printf("cmd[% 02X]", buf)
			answ, err := txRx(*port, *speedConn, buf)
			if err == nil {
				fmt.Printf(" Reply PD[%d] [% 02X]\n", i, answ)
				//parse reply
			}
		}
		os.Exit(0)
	}

	buf, err := makePacket(uint8(*idCur), *speedSet)
	if err != nil {
		log.Fatalf("ERROR: %s\n", err)
	}

	fmt.Printf("cmd[% 02X]\n", buf)
	answ, err := txRx(*port, *speedConn, buf)
	if err != nil {
		log.Fatalf("ERROR: %s\n", err)
	}

	fmt.Printf("Got reply from PD[%d] [% 02X]\n", *idCur, answ)

}

func showInfo() {
	fmt.Printf("%s\n ", version)
}

func txRx(name string, baud int, data []byte) ([]byte, error) {
	speedSets := []int{1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200}
	if *autoSpeed {

		fmt.Printf("Start Auto Speed Mode\n")
		for _, speed := range speedSets {

			fmt.Printf("Test Speed - %d\n", speed)
			dt, err := communicate(name, speed, data)

			if l := len(dt); l > 1 {
				return dt, nil
			}

			if err != nil {
				return nil, fmt.Errorf("txRx: %s", err)
			}
		}
		return nil, fmt.Errorf("No Data returned")
	}
	return communicate(name, baud, data)
}

func communicate(name string, baud int, data []byte) ([]byte, error) {
	c := &serial.Config{Name: name, Baud: baud}

	//TODO Timeout based on baud rate
	c.ReadTimeout = time.Millisecond * 500

	s, err := serial.OpenPort(c)
	if err != nil {
		return nil, fmt.Errorf("communicate: Open port[%s]: %s", name, err)
	}
	defer s.Close()

	n, err := s.Write(data)
	if err != nil {
		return nil, fmt.Errorf("communicate: Write: %s", err)
	}

	buf := make([]byte, 128)
	n, err = s.Read(buf)
	if err != nil {
		return nil, fmt.Errorf("communicate: Read: %s", err)
	}
	return buf[:n], nil
}

func checkValues() error {
	if *idCur > 0x7E {
		return fmt.Errorf("PD ID can't be more then 126")
	}
	return nil
}

func makePacket(id byte, speed int) ([]byte, error) {
	p := osdp.New()
	p.Addr = id

	if *crcMode == false {
		p.BitCRC16Clear()
	}

	data := osdp.ComSetStruct{
		Address:  byte(*idSet),
		BaudRate: uint32(speed),
	}
	return p.CPMakePacket(id, osdp.Osdp_COMSET, false, osdp.SqnClear, data)
}
